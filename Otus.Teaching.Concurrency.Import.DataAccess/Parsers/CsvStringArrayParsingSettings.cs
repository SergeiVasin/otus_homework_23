﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class CsvStringArrayParsingSettings<T> where T : class
    {
        public CsvStringArrayParsingSettings(
            List<T> dataList,
            string[] strings,
            string separator,
            int startIndex,
            int finishIndex,
            bool skipHeaders = false,
            IEnumerable<string> headers = null
            )
        {
            DataList = dataList;
            Strings = strings;
            Separator = separator;
            StartIndex = startIndex;
            FinishIndex = finishIndex;
            SkipHeaders = skipHeaders;
            Headers = headers;
        }
        public List<T> DataList { get; private set; }// парсер заполняет эту коллекцию
        public string[] Strings { get; private set; }
        public int StartIndex { get; private set; }
        public int FinishIndex { get; private set; }
        public bool SkipHeaders { get; private set; }
        public IEnumerable<string> Headers { get; private set; }
        public string Separator { get; private set; }
    }
}
