﻿using Csv;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class CsvStringArrayParser<T> : IDataParser<List<T>> where T : class
    {
        private readonly CsvStringArrayParsingSettings<T> _parsingSettings;

        public CsvStringArrayParser(
            CsvStringArrayParsingSettings<T> parsingSettings
            )
        {
            _parsingSettings = parsingSettings;
        }

        public List<T> Parse()
        {
            var serializer = new CsvSerializer();

            var collection = new List<T>();

            for (int i = _parsingSettings.StartIndex; i <= _parsingSettings.FinishIndex; i++)
            {
                collection.Add(
                    serializer.Deserialize<T>(
                        _parsingSettings.Strings[i],
                        _parsingSettings.Separator,
                        _parsingSettings.Headers
                        )
                    );
            }
            return collection;
        }

    }
}
