﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.DataAccess
{
    public class DbWritingSettings<T> where T : class
    {
        public DbWritingSettings(
            List<T> DataList,
            int startIndex,
            int finishIndex
            )
        {
            this.DataList = DataList;
            StartIndex = startIndex;
            FinishIndex = finishIndex;
        }

        public List<T> DataList { get; private set; }
        public int StartIndex { get; private set; }
        public int FinishIndex { get; private set; }
    }
}
