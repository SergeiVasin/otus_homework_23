﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.DataAccess
{
    public class DbInitializer
    {
        private readonly AppDbContext _dataContext;

        public DbInitializer(AppDbContext dataContext)
        {
            _dataContext = dataContext;
        }
        public void Initialize()
        {
            _dataContext.Database?.EnsureDeleted();
            _dataContext.Database.EnsureCreated();
        }
    }
}
