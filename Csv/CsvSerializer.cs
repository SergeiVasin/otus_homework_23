﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Text;

namespace Csv
{
    public class CsvSerializer
    {
        public static void Serialize<T>(string separator, string filePath, IEnumerable<T> objList)
        {
            string str = Serialize(separator, objList);
            WriteToCsvFile(filePath, str);
        }

        public static string Serialize<T>(string separator, IEnumerable<T> objList)
        {
            if (objList == null) return null;
            bool isFirstObj = true;
            var stringBuilder = new StringBuilder();
            List<string> csvHeadersStr = new List<string>();


            foreach (var obj in objList)
            {
                stringBuilder.Append("\"");

                var propArr = obj.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);
                List<string> strArr = new List<string>();


                foreach (var pi in propArr)
                {
                    if (pi.PropertyType.BaseType == typeof(object) && pi.PropertyType != typeof(string)) continue;

                    if (isFirstObj)
                        csvHeadersStr.Add(pi.Name);

                    strArr.Add(pi.GetValue(obj)?.ToString());
                }

                if (isFirstObj)
                {
                    stringBuilder.Append(string.Join(separator, csvHeadersStr));
                    stringBuilder.Append("\"\r\n");
                    stringBuilder.Append("\"");
                }

                isFirstObj = false;

                stringBuilder.Append(string.Join(separator, strArr));

                stringBuilder.Append("\"\r\n");

            }
            return stringBuilder.ToString();
        }

        private static void WriteToCsvFile(string filePath, string str)
        {
            using var stream = File.Create(filePath);
            byte[] buffer = Encoding.Default.GetBytes(str);
            stream.Write(buffer, 0, buffer.Length);
        }

        public static List<T> Deserialize<T>(string filePath, string separator, bool skipHeaders = false, IEnumerable<string> headers = null)
        {
            string str = ReadFile(filePath);
            var strObjArr = str.Trim().Split("\r\n");
            var objList = new List<T>();

            bool skipHeaderStr = skipHeaders;
            List<string> iHeaders = new List<string>();
            if (skipHeaderStr)
            {
                iHeaders.AddRange(headers);
                skipHeaderStr = false;
            }

            Type type = typeof(T);

            foreach (var item in strObjArr)
            {
                var sArr = item.Substring(1, item.Length - 2).Split(separator);

                if (!skipHeaderStr)
                {
                    iHeaders.AddRange(sArr);
                    skipHeaderStr = true;
                    continue;
                }

                var newObj = (T)Activator.CreateInstance(type);

                for (int i = 0; i < iHeaders.Count; i++)
                {
                    var pInfo = newObj.GetType().GetProperty(iHeaders[i]);
                    Type t = pInfo.PropertyType;
                    TypeConverter conv = TypeDescriptor.GetConverter(t);

                    var convertResult = conv.ConvertFrom(sArr[i]);

                    pInfo.SetValue(newObj, convertResult);
                }

                objList.Add(newObj);
            }

            return objList;
        }

        public T Deserialize<T>(string str, string separator, IEnumerable<string> headers)
        {
            var newObj = (T)Activator.CreateInstance(typeof(T));
            var sArr = str.Substring(1, str.Length - 2).Split(separator);

            int i = 0;
            foreach (var header in headers)
            {
                var pInfo = newObj.GetType().GetProperty(header);
                Type t = pInfo.PropertyType;

                TypeConverter conv = TypeDescriptor.GetConverter(t);
                var convertResult = conv.ConvertFrom(sArr[i]);

                pInfo.SetValue(newObj, convertResult);

                i++;
            }
            return newObj;
        }

        private static string ReadFile(string filePath)
        {
            using var stream = new FileStream(filePath, FileMode.Open);
            byte[] buffer = new byte[stream.Length];
            stream.Read(buffer, 0, buffer.Length);

            return Encoding.Default.GetString(buffer);

        }

    }
}
