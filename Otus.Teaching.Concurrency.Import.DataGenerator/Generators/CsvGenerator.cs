﻿using Csv;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class CsvGenerator : IDataGenerator
    {
        private readonly string _filePath;
        private readonly int _dataCount;
        private readonly string _separator;

        public CsvGenerator(string filePath, string separator, int dataCount)
        {
            _filePath = filePath;
            _dataCount = dataCount;
            _separator = separator;
        }
        public void Generate()
        {
            var customers = RandomCustomerGenerator.Generate(_dataCount);

            CsvSerializer.Serialize(_separator, _filePath, customers);
        }
    }
}
