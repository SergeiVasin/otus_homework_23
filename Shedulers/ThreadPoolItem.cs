﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Shedulers
{
    public class ThreadPoolItem
    {
        public int Id { get; private set; }

        public object Data { get; private set; }

        public WaitHandle WaitHandle { get; private set; }

        public ThreadPoolItem(int id, object data)
        {
            Id = id;
            Data = data;
            WaitHandle = new AutoResetEvent(false);
        }
    }
}
