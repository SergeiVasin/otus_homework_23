﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shedulers
{
    public interface IScheduler
    {
        void ProcessQueue();
    }
}
