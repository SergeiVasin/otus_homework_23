﻿using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using System;
using System.Diagnostics;
using System.IO;

namespace Homework_23
{
    internal class Program
    {
        private static string _separator = ";";
        private static int _dataAmount = 1000000;

        private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers");
        private const string HandlerGeneratingProcessFileName = "Otus.Teaching.Concurrency.Import.DataGenerator.App.exe";
        private static string HandlerGeneratingProcessDirectory = 
            @$"{AppDomain.CurrentDomain.BaseDirectory}..\..\..\..\Otus.Teaching.Concurrency.Import.DataGenerator.App\bin\Debug\netcoreapp3.1";

        private const string HandlerLoadingProcessFileName = "Otus.Teaching.Concurrency.Import.Loader.exe";
        private static string HandlerLoadingProcessDirectory = 
            @$"{AppDomain.CurrentDomain.BaseDirectory}..\..\..\..\Otus.Teaching.Concurrency.Import.Loader\bin\Debug\netcoreapp3.1";

        static void Main(string[] args)
        {
            var dll = AppDomain.CurrentDomain.GetAssemblies();

            //Дз п 1, 5 (генерируем данные(через метод или процесс) и записываем в файл)
            var inputRes = ChooseCommandForGenerateCustomers();
            if (inputRes == "1")
            {
                GenerateCustomersDataFile();
            }
            if (inputRes == "2")
            {
                var proc = StartGeneratingProcess(_dataFilePath);
                proc.WaitForExit();
            }

            //Дз п 2, 3(парсим данные из файла(однопоточно или многопоточно) и записываем в БД(однопоточно или многопоточно))
            string parsingType = ChooseCommandForParsing();
            string dbWritingType = ChooseCommandForDbWriting();
            StartLoadingProcess(parsingType, dbWritingType).WaitForExit();
        }

        //Дз п 1, 5
        static string ChooseCommandForGenerateCustomers()
        {
            while (true)
            {
                Console.WriteLine("Генерация файла csv.");
                Console.WriteLine("Выберите команду.");
                Console.WriteLine("----------------");
                Console.WriteLine("1. С помощью метода.");
                Console.WriteLine("2. С помощью процесса.");
                Console.WriteLine("3. Не выполнять генерацию данных.");

                string choiceResult = Console.ReadLine();
                if (choiceResult == "1" || choiceResult == "2" || choiceResult == "3")
                {
                    return choiceResult;
                }

                Console.WriteLine("Введите номер команды!");
                Console.WriteLine();
            }
        }

        //Дз п 1, 5
        static void GenerateCustomersDataFile()
        {
            Console.WriteLine("Generating csv data...");

            var csvGenerator = new CsvGenerator(_dataFilePath, _separator, _dataAmount);
            csvGenerator.Generate();

            Console.WriteLine($"Generated csv data in {_dataFilePath}.csv ...");
        }

        //Дз п 1, 5
        static Process StartGeneratingProcess(string path)
        {
            var startInfo = new ProcessStartInfo()
            {
                ArgumentList = { path, _separator, _dataAmount.ToString() },
                FileName = GetPathToHandlerProcess(HandlerGeneratingProcessDirectory, HandlerGeneratingProcessFileName),
            };

            var process = Process.Start(startInfo);
            Console.WriteLine($"Generating process Id: {process.Id}");

            return process;
        }

        //Дз п 2, 3
        static string ChooseCommandForParsing()
        {
            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("Парсинг файла csv.");
                Console.WriteLine("Выберите команду.");
                Console.WriteLine("----------------");
                Console.WriteLine("1. Однопоточный парсинг.");
                Console.WriteLine("2. Многопоточный парсинг.");

                string choiceResult = Console.ReadLine();
                if (choiceResult == "1" || choiceResult == "2")
                {
                    return choiceResult;
                }

                Console.WriteLine("Введите номер команды!");
                Console.WriteLine();
            }
        }

        //Дз п 2, 3
        static string ChooseCommandForDbWriting()
        {
            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("Запись данных в базу данных.");
                Console.WriteLine("Выберите команду.");
                Console.WriteLine("----------------");
                Console.WriteLine("1. Однопоточная запись.");
                Console.WriteLine("2. Многопоточная запись.");

                string choiceResult = Console.ReadLine();
                if (choiceResult == "1" || choiceResult == "2")
                {
                    return choiceResult;
                }

                Console.WriteLine("Введите номер команды!");
                Console.WriteLine();
            }
        }

        //Дз п 2, 3
        static Process StartLoadingProcess(string parsingType, string dbWritingType)
        {

            var startInfo = new ProcessStartInfo()
            {
                ArgumentList = { _dataFilePath, _separator, "true", parsingType, dbWritingType },
                FileName = GetPathToHandlerProcess(HandlerLoadingProcessDirectory, HandlerLoadingProcessFileName),
            };

            var process = Process.Start(startInfo);
            Console.WriteLine($"Loading process Id: {process.Id}");
            Console.WriteLine();

            return process;
        }

        static string GetPathToHandlerProcess(string directory, string fileName)
        {
            return Path.Combine(directory, fileName);
        }

    }
}
