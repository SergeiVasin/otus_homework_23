﻿using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Handlers
{
    public class DbWritingHandler<T> : IHandler where T : class
    {
        private readonly DbWritingSettings<T> _data;

        public DbWritingHandler(object data)
        {
            _data = (DbWritingSettings<T>)data;
        }
        public void Handle()
        {
            using AppDbContext context = new AppDbContext();
            var repo = new Repository<T>(context);

            //В TaskScheduler есть retry механизм перезапуска в случае сбоя - изучить и применить
            //описать схемой чтобы понять что происходит

            var col = new List<T>();

            for (int i = _data.StartIndex; i <= _data.FinishIndex; i++)
            {
                col.Add(_data.DataList[i]);

            }

            repo.AddRange(col);

            var saveRes = false;

            while (!saveRes)
            {
                try
                {
                    repo.Complete();
                    saveRes = true;
                }
                catch
                {
                    //Указываем кол-во неудачных попыток(Дз п.2)
                    RetryCounter.Retry();
                    Console.WriteLine($"Attempt to write to database # {RetryCounter.Counter}");
                    continue;
                }
            }

        }
    }
}
