﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Handlers
{
    public interface IHandler
    {
        void Handle();
    }
}
