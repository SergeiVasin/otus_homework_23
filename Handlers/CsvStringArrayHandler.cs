﻿using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using System;
using System.Collections.Generic;
using System.Reflection.Metadata;
using System.Text;

namespace Handlers
{
    public class CsvStringArrayHandler<T> : IHandler where T : class
    {
        private readonly object _data;

        public CsvStringArrayHandler(object data) 
        {
            _data = data;
        }
        public void Handle()
        {
            var data = (CsvStringArrayParsingSettings<T>)_data;

            var parser = new CsvStringArrayParser<T>(data);
            data.DataList.AddRange(parser.Parse());

        }
    }
}
