﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Handlers
{
    public class HandlerFactory
    {
        public static IHandler GetCsvStringArrayHandler<T>(object data) where T : class
        {
            var handler = new CsvStringArrayHandler<T>(data);

            return handler;
        }

        public static IHandler GetDbWritingHandler<T>(object data) where T : class
        {
            var handler = new DbWritingHandler<T>(data);

            return handler;
        }

    }
}
