﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Handlers
{
    internal class RetryCounter
    {
        internal static int Counter { get; private set; }
        internal static void Retry()
        {
            Counter++;
        }
    }
}
