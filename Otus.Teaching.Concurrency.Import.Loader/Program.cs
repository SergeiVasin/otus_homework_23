﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Handlers;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Shedulers;


namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static string _dataFilePath = 
            @$"{AppDomain.CurrentDomain.BaseDirectory}..\..\..\..\Homework_23\bin\Debug\netcoreapp3.1\customers.csv";
        private static string _separator = ";";
        private static bool _skipHeaders = true;
        private static string _parsingType = "2";
        private static string _dbWritingType = "1";
        private static int _cores = Environment.ProcessorCount;

        static void Main(string[] args)
        {
            if (!TryValidateAndParseArgs(args))
                return;

            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");
            Console.WriteLine($"Путь к файлу {_dataFilePath}");

            List<Customer> customers = null;

            Console.WriteLine("Parsing started...");
            var parseTimeSpan = DoBenchmark(() =>
            {
                customers = ParseCsvFile();
            });
            Console.WriteLine($"Parsing finished. Elapsed time = {parseTimeSpan}");

            Console.WriteLine("Loading data to Db started...");
            var dbWritingTimeSpan = DoBenchmark(() =>
            {
                AddCustomersToDb(customers);
            });
            Console.WriteLine($"Loading data to Db finished. Elapsed time = {dbWritingTimeSpan}");
        }

        public static List<Customer> ParseCsvFile()
        {
            List<Customer> customers = new List<Customer>();
            try
            {
                if (_parsingType == "2")//многопоточный парсинг
                {
                    List<List<Customer>> customersChunkList = new List<List<Customer>>();
                    
                    string fileStr = ReadFile();

                    var strArr = fileStr.Trim().Split("\r\n");

                    var headers = new List<string>();
                    headers.AddRange(strArr[0].Substring(1, strArr[0].Length - 2).Split(_separator));

                    strArr = strArr.Skip(1).ToArray();//удаляем заголовки

                    int range = strArr.Length / _cores;

                    var dataSettingsObjects = new List<CsvStringArrayParsingSettings<Customer>>();

                    var startInd = 0;

                    for (int i = 0; i < _cores; i++)
                    {
                        var chunkOfCustomers = new List<Customer>();//коллекция для заполнения парсером
                        customersChunkList.Add(chunkOfCustomers);

                        if (i == _cores - 1)
                        {
                            dataSettingsObjects.Add(new CsvStringArrayParsingSettings<Customer>(
                                chunkOfCustomers, strArr, _separator, startInd, strArr.Length - 1, true, headers));
                            break;
                        }
                        dataSettingsObjects.Add(new CsvStringArrayParsingSettings<Customer>(
                            chunkOfCustomers, strArr, _separator, startInd, startInd - 1 + range, true, headers));

                        startInd += range;
                    }

                    var schedulerForFileReading = new ThreadScheduler(dataSettingsObjects, (o) =>
                    {
                        var handler = HandlerFactory.GetCsvStringArrayHandler<Customer>(o);

                        handler.Handle();
                    });
                    schedulerForFileReading.ProcessQueue();

                    foreach (var cl in customersChunkList)//объединяем чанки в общую коллекцию
                    {
                        customers.AddRange(cl);
                    }
                }
                else
                {
                    //однопоточный парсинг
                    var parser = new CsvFileParser<Customer>(_dataFilePath, _separator);
                    customers = parser.Parse();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }

            return customers;
        }

        public static void AddCustomersToDb(List<Customer> customers)
        {
            try
            {
                using var dataContext = new AppDbContext();
                new DbInitializer(dataContext).Initialize();

                if (_dbWritingType == "2")//многопоточная запись в БД
                {
                    int rangeForDbWriting = customers.Count / _cores;

                    var dbWritingSettingsObjects = new List<DbWritingSettings<Customer>>();

                    var startIndForDbWriting = 0;

                    for (int i = 0; i < _cores; i++)
                    {
                        if (i == _cores - 1)
                        {
                            dbWritingSettingsObjects.Add(new DbWritingSettings<Customer>(customers, startIndForDbWriting, customers.Count - 1));
                            break;
                        }
                        dbWritingSettingsObjects.Add(new DbWritingSettings<Customer>(customers, startIndForDbWriting, startIndForDbWriting - 1 + rangeForDbWriting));

                        startIndForDbWriting += rangeForDbWriting;
                    }

                    var schedulerForDbWriting = new ThreadScheduler(dbWritingSettingsObjects, (o) =>
                    {
                        var handler = HandlerFactory.GetDbWritingHandler<Customer>(o);

                        handler.Handle();
                    });
                    schedulerForDbWriting.ProcessQueue();
                }
                else//однопоточная запись в БД
                {
                    var repo = new Repository<Customer>(dataContext);

                    repo.AddRange(customers);

                    var dbWriingResult = repo.Complete();
                    if(dbWriingResult <= 0)
                    {
                        Console.WriteLine("Failed to add data to database.");
                        return;
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }
        }

        private static string ReadFile()
        {
            using var stream = new FileStream(_dataFilePath, FileMode.Open);
            byte[] buffer = new byte[stream.Length];
            stream.Read(buffer, 0, buffer.Length);

            return Encoding.Default.GetString(buffer);
        }

        private static bool TryValidateAndParseArgs(string[] args)
        {
            if (args != null && args.Length == 5)
            {
                _dataFilePath = $"{args[0]}.csv";

                if (args[1] != ";" && args[1] != ",")
                {
                    Console.WriteLine("Separator must be comma or semicolon.");
                    return false;
                }

                if (!Boolean.TryParse(args[2], out _skipHeaders))
                {
                    Console.WriteLine("Skip headers value is required.");
                    return false;
                }

                if (args[3] != "1" && args[3] != "2")
                {
                    Console.WriteLine("Parsing type must be 1 or 2.");
                    return false;
                }
                _parsingType = args[3];

                if (args[4] != "1" && args[4] != "2")
                {
                    Console.WriteLine("Database writing type must be 1 or 2.");
                    return false;
                }
                _dbWritingType = args[4];
            }
            else
            {
                Console.WriteLine("Not enough data for Loader.");
                return false;
            }

            return true;
        }

        public static TimeSpan DoBenchmark(Action action)
        {
            var sw = new Stopwatch();
            sw.Start();

            action();

            sw.Stop();

            return sw.Elapsed;
        }
    }
}
